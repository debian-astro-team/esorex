Source: esorex
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               help2man,
               libcfitsio-dev,
               libcpl-dev,
               libffi-dev,
               libltdl-dev,
               wcslib-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian-astro-team/esorex
Vcs-Git: https://salsa.debian.org/debian-astro-team/esorex.git
Homepage: https://www.eso.org/sci/software/cpl/esorex.html
Rules-Requires-Root: no

Package: esorex
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Execution Tool for European Southern Observatory pipelines
 EsoRex  is the European Southern Observatory (ESO) Recipe Execution Tool. It
 can list, configure and execute CPL-based recipes from the command line.
 .
 The Common Pipeline Library (CPL) comprises a set of ISO-C libraries that
 provide a comprehensive, efficient and robust software toolkit. It forms a
 basis for the creation of automated astronomical data-reduction tasks.
 .
 One of the features provided by the CPL is the ability to create
 data-reduction algorithms that run as plugins (dynamic libraries). These are
 called "recipes" and are one of the main aspects of the CPL data-reduction
 development environment.
